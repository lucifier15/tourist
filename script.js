 var app = angular.module('flynote-app',["ngRoute"]);
 app.controller('Form-Controller',['$scope','$http','$timeout',function($scope,$http,$timeout){
 	$scope.user = {
 		phone: '',
 		email : '',
 		avatar : ''
 	};

 	$scope.submit = function(){
 		var email = $scope.user.email;
		 var reg = /^([\w-\.]+@(?!gmail.com)(?!yahoo.com)(?!hotmail.com)(?!yahoo.co.in)(?!aol.com)(?!abc.com)(?!xyz.com)(?!pqr.com)(?!rediffmail.com)(?!live.com)(?!outlook.com)(?!me.com)(?!msn.com)(?!ymail.com)([\w-]+\.)+[\w-]{2,4})?$/;
		  if (reg.test(email)){
		 }
		 else{
		 $scope.email_msg = 'Enter a valid business email';
		 $scope.isCheck2 = true;
 		 $timeout(function(){ $scope.isCheck2 = false },5000);
		 return false;
		 }	

			$http({
 			method : 'POST',
 			url    : 'https://about2fly.flynote.in/api/admin/intern',
 			headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
 			transformRequest: function(obj) {
	        var str = [];
	        for(var p in obj)
	        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	        return str.join("&");
	    },
 			data   : {phone: $scope.user.phone, email: $scope.user.email,avatar: $scope.user.avatar},
 		})
 		.then(function(response){
 			if(response.data.state == true){
 				$scope.message = response.data.message,
 				console.log(response);
 				$scope.isCheck1 = true;
 				$timeout(function(){ $scope.isCheck1 = false },7000);
 				$scope.user = {};
 			}
 			else{
 				$scope.err_msg = 'Invalid Entries',
 				console.log(response),
 				$scope.isCheck = true;
 				$timeout(function(){ $scope.isCheck = false },7000);
 			}
 		});
 	}
 }]);
 app.config(function($routeProvider){
 	$routeProvider
 	.when("/",{
 		templateUrl: 'home.html'
 	})
 	.when("/about",{
 		templateUrl : "about.html"
 	});
 });